<?php

namespace Helperx;

use App\Model\DataAKKE;
use App\Model\DataKeluaran;
use Illuminate\Http\Request;
use App\Model\DataProvider;
use Illuminate\Support\Str;

use Illuminate\Support\Facades\DB;

class AngkaDetect
{


    static $datapermutasi;
    public function hitungAngka($angkaKeluar)
    {
        $angkaKeluar =  (string) str_pad($angkaKeluar, 4, '0', STR_PAD_LEFT);
        return [
            'AngkaKeluar' => $angkaKeluar,
            '_report' => [
                '_shio' => $this->detectShio($angkaKeluar),
                '_akke' => $this->detectAkke($angkaKeluar),
                // '_dasar' => $this->detectDasar($angkaKeluar),
                '_colokBebas' => $this->detectColokBebas($angkaKeluar),
                '_5050' => $this->detectUmum($angkaKeluar),

            ]
        ];
    }


    public function hitungData($data)
    {
        // '_umum'=>$this->detectUmum($angkaKeluar),
        //         '_kombinasi'=>$this->detectKombinasi($angkaKeluar),
    }
    public function detectKombinasi($angkaKeluar)
    {
    }

    public function detectUmum($angkaKeluar)
    {
        $stmt = [];
        $stmt_kom = [];
        $angka3 = substr($angkaKeluar, 2);
        $angka4 = substr($angkaKeluar, 3);
        if ($angka3 < 50) {
            //masuk kecil
            $stmt[] .= 'KECIL';
            if ($angka3 % 2 == 0) {
                // habis dibagi 2 = genap
                $stmt_kom[] .= 'KECIL GENAP';
            } else {
                // kecil ganjil
                $stmt_kom[] .= 'KECIL GANJIL';
            }
        } else {
            // masuk besar
            $stmt[] .= 'BESAR';
            if ($angka3 % 2 == 0) {
                // masuk besar genap
                $stmt_kom[] .= 'BESAR GENAP';
            } else {
                // masuk besar ganjil
                $stmt_kom[] .= 'BESAR GANJIL';
            }
        }



        if ($angka4 % 2 == 0) {
            $stmt[] .= 'GENAP';
        } else {

            $stmt[] .= 'GANJIL';
        }

        return [
            'umum_' => $stmt,
            'kombinasi_' => $stmt_kom,

        ];
        // return implode(',', $stmt);
        // return implode(' ',$stmt);

    }
    private function detectColokBebas($angkaKeluar)
    {
        $res = '';
        for ($i = 0; $i < 10; $i++) {
            $res .= substr_count($angkaKeluar, $i) . ',';
            # code...
        }
        return rtrim($res, ',');
    }
    private function detectAkke($keluaran)
    {
        if (strlen($keluaran) > 4) {
            return 'Keluaran tidak boleh lebih dari 4';
        }
        $keluaran =  str_split($keluaran);
        return [
            'AS' => $keluaran[0],
            'KOP' => $keluaran[1],
            'KEPALA' => $keluaran[2],
            'EKOR' => $keluaran[3],
        ];
    }


    private function detectDasar($keluaran)
    {
        if ($keluaran % 2 == 0) {
            return 'GANJIL';
        }
        return 'GENAP';
    }
    private function detectShio($angka)
    {
        $angka = substr($angka, 2);
        $angka = str_pad($angka, 2, '0', STR_PAD_LEFT);

        $arrayShio = [
            "01" => "TIKUS", "02" => "BABI", "03" => "ANJING", "04" => "AYAM", "05" => "MONYET", "06" => "KAMBING", "07" => "KUDA", "08" => "ULAR", "09" => "NAGA", "10" => "KELINCI", "11" => "HARIMAU", "12" => "KERBAU", "13" => "TIKUS", "14" => "BABI", "15" => "ANJING", "16" => "AYAM", "17" => "MONYET", "18" => "KAMBING", "19" => "KUDA", "20" => "ULAR", "21" => "NAGA", "22" => "KELINCI", "23" => "HARIMAU", "24" => "KERBAU", "25" => "TIKUS", "26" => "BABI", "27" => "ANJING", "28" => "AYAM", "29" => "MONYET", "30" => "KAMBING", "31" => "KUDA", "32" => "ULAR", "33" => "NAGA", "34" => "KELINCI", "35" => "HARIMAU", "36" => "KERBAU", "37" => "TIKUS", "38" => "BABI", "39" => "ANJING", "40" => "AYAM", "41" => "MONYET", "42" => "KAMBING", "43" => "KUDA", "44" => "ULAR", "45" => "NAGA", "46" => "KELINCI", "47" => "HARIMAU", "48" => "KERBAU", "49" => "TIKUS", "50" => "BABI", "51" => "ANJING", "52" => "AYAM", "53" => "MONYET", "54" => "KAMBING", "55" => "KUDA", "56" => "ULAR", "57" => "NAGA", "58" => "KELINCI", "59" => "HARIMAU", "60" => "KERBAU", "61" => "TIKUS", "62" => "BABI", "63" => "ANJING", "64" => "AYAM", "65" => "MONYET", "66" => "KAMBING", "67" => "KUDA", "68" => "ULAR", "69" => "NAGA", "70" => "KELINCI", "71" => "HARIMAU", "72" => "KERBAU", "73" => "TIKUS", "74" => "BABI", "75" => "ANJING", "76" => "AYAM", "77" => "MONYET", "78" => "KAMBING", "79" => "KUDA", "80" => "ULAR", "81" => "NAGA", "82" => "KELINCI", "83" => "HARIMAU", "84" => "KERBAU", "85" => "TIKUS", "86" => "BABI", "87" => "ANJING", "88" => "AYAM", "89" => "MONYET", "90" => "KAMBING", "91" => "KUDA", "92" => "ULAR", "93" => "NAGA", "94" => "KELINCI", "95" => "HARIMAU", "96" => "KERBAU", "97" => "TIKUS", "98" => "BABI", "99" => "ANJING", "00" => "AYAM",

        ];

        return $arrayShio[$angka];
    }

    private function buildArrayShio()
    {
        $shio = [
            'TIKUS',
            'BABI',
            'ANJING',
            'AYAM',
            'MONYET',
            'KAMBING',
            'KUDA',
            'ULAR',
            'NAGA',
            'KELINCI',
            'HARIMAU',
            'KERBAU',
        ];
        $counterShio = 0;
        for ($i = 1; $i <= 100; $i++) {
            $val = str_pad($i, 2, '0', STR_PAD_LEFT);


            echo '"' . $val . '"' . " => " . '"' . $shio[$counterShio] . '"' . ", ";
            if ($counterShio == 11) {
                $counterShio = 0;
            } else {
                $counterShio++;
            }
        }
    }
    public static function processPola2D($id)
    {
        $data = DB::table('data_keluaran')
            ->whereIn('id', $id)
            ->orderBy('tanggal', 'DESC')
            ->limit(10)
            ->get();


        $res = [];
        foreach ($data as $key => $value) {
            //batasi 10
            if ($key < 10) {
                // do something here
                $hasil =  AngkaDetect::genapGanjil($value->keluaran);
                $res[$value->keluaran] = $hasil;


                // // array_push($sm,$value->keluaran);
                // $array_hasil = $hasil;
                // // // $array_hasil = explode(', ', $hasil);
                // foreach ($array_hasil as $val) {
                //     $res[$val] = $res[$val] + 1;
                // }
            }
        }
        return $res;
    }


    public static function hitungPrediksi($provider, $tanggal)
    {
        $dateReverse = explode('-', $tanggal);
        $tanggal = implode('-', array_reverse($dateReverse));

        $providerData = DataProvider::where('nama', 'like', '%' . $provider . '%')->first();
        $periodeSebelumnya = DataKeluaran::where('provider', $providerData->id)
            ->whereDate('tanggal', '<', $tanggal)
            ->orderBy('tanggal', 'DESC')->limit(2)->get();


        $datasebelumnya5 = DataKeluaran::select('tanggal', 'hari')
            ->where('provider', $providerData->id)
            ->whereDate('tanggal', '<', $tanggal)
            ->orderBy('tanggal', 'DESC')->limit(5)->get();


        $last30DaysData =   DataKeluaran::select(
            'id',

        )->where('provider', $providerData->id)
            ->orderBy('tanggal', 'DESC')
            ->limit(30)
            ->get();
        $last30daysDataID =  $last30DaysData->pluck('id');


        // $angkaMain = '1888';
        $angkaMain = self::hitungAngkaMain($periodeSebelumnya);
        $res['provider'] = $providerData;
        $res['periode_sebelumnya5']  = $datasebelumnya5;
        $res['periode_sebelumnya']  = $periodeSebelumnya;
        // $shioAngkaMain = self::cariShioAngkaMain($angkaMain);
        // $res['periode_sebelumnya'] = $periodeSebelumnya;


        self::permutasi(self::arrayAngka($angkaMain));
        $res['angka_main']  = $angkaMain;

        $res['prediksi_pola2d']  = self::processPola2D($last30daysDataID);
        $res['prediksi_shio']  = self::cariShioAngkaMain($angkaMain);
        $res['prediksi_dasar']  = self::cariDasarAngkaMain($angkaMain);
        $res['prediksi_ekor']  = self::cariEkorAngkaMain($angkaMain);
        $res['prediksi_colok_bebas']  = self::cariColokBebasAngkaMain($angkaMain);
        $res['prediksi_colok_macau']  = self::cariColokMacauAngkaMain($angkaMain);
        $res['prediksi_anjuran_bolak_balik']  = self::cariAnjuranBolakBalikAngkaMain($angkaMain);
        $res['prediksi_2d']  = self::olahPermutasi(2);
        $res['prediksi_3d']  = self::olahPermutasi(3);
        $res['prediksi_4d']  = self::olahPermutasi(4);
        $res['prediksi_askop']  = self::olahAskop($last30daysDataID);

        // return $res;
        try {
            return response([
                'success' => true,
                'data' => $res
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage(),
                ]
            ], 200);
        }
    }



    public static function olahAskop($id)
    {
        $as  =  DB::table('data_akke')
            ->select(
                DB::raw('`as` as nomor, COUNT(*) AS `jlh` ')
            )->whereIn('id_keluaran', $id)
            ->groupBy('nomor')
            // ->orderBy(DB::raw('count(`as`)'), 'DESC')
            ->get();
        // dd(DB::getQueryLog());
        $kop  =  DB::table('data_akke')
            ->select(
                DB::raw('`kop` as nomor, COUNT(*) AS `jlh` ')
            )->whereIn('id_keluaran', $id)
            ->groupBy('nomor')
            // ->orderBy(DB::raw('count(`as`)'), 'DESC')
            ->get();

        $kepala  =  DB::table('data_akke')
            ->select(
                DB::raw('`kepala` as nomor, COUNT(*) AS `jlh` ')
            )->whereIn('id_keluaran', $id)
            ->groupBy('nomor')
            // ->orderBy(DB::raw('count(`as`)'), 'DESC')
            ->get();

        $ekor  =  DB::table('data_akke')
            ->select(
                DB::raw('`ekor` as nomor, COUNT(*) AS `jlh` ')
            )->whereIn('id_keluaran', $id)
            ->groupBy('nomor')
            // ->orderBy('ekor', 'DESC')
            ->get();


        // return $this->filterAskor($ekor);

        // return self::olahAskopArray($as);
        return [
            'as' => self::filterAskop($as),
            'kop' => self::filterAskop($kop),
            'kepala' => self::filterAskop($kepala),
            'ekor' => self::filterAskop($ekor),
        ];
    }



    public static function filterAskop($array)
    {
        // return $array;
        $sementara = [];

        // $sementara['data'] = [];
        $sementara['jumlahganjil'] = 0;
        $sementara['jumlahgenap'] = 0;
        $sementara['jumlahkiri'] = 0;
        $sementara['jumlahkanan'] = 0;
        $sementara['kiri'] = '';
        $sementara['kanan'] = '';
        // $sementara['raw'] = $array;
        $statusKiriCounterKecil = 0;
        $statusKiriCounterBesar = 0;
        $statusKananCounterKecil = 0;
        $statusKananCounterBesar = 0;
        for ($i = 0; $i < 10; $i++) {

            $sementara['data'][$i] = 0;
        }
        $x = 0;
        $jumlahKiri = 0;
        $jumlahKanan = 0;
        $jumlahGenap = 0;
        $jumlahGanjil = 0;

        $besar = (object) ['nomor' => 0, 'jlh' => 0];
        $kecil = (object) ['nomor' => 9, 'jlh' => 9];


        $valBesar = 0;
        $valKecil = 0;
        // foreach ($array as $d) {

        //     if ($d->jlh > $besar->jlh) {
        //         $besar = $d;
        //     }
        //     if ($d->jlh < $kecil->jlh) {
        //         $kecil = $d;
        //     }
        // }

        foreach ($array as $key => $kor) {
            if ($kor->nomor % 2 == 0) {
                $jumlahGenap  = $jumlahGenap + $kor->jlh;
            } else {
                $jumlahGanjil =  $jumlahGanjil + $kor->jlh;
            }
            if ($kor->nomor < 5) {
                $jumlahKiri  = $jumlahKiri + $kor->jlh;
            } else {
                $jumlahKanan  = $jumlahKanan + $kor->jlh;
            }

            if ($kor->jlh > $besar->jlh) {
                $besar = $kor;
            }
            if ($kor->jlh < $kecil->jlh) {
                $kecil = $kor;
            }

            $sementara['data'][$kor->nomor] = $kor->jlh;
        }
        $sementara['seringg'] = $besar->nomor;
        $sementara['jarang'] = $kecil->nomor;
        $sementara['jumlahkiri'] = $jumlahKiri;
        $sementara['jumlahkanan'] = $jumlahKanan;

        if ($jumlahGanjil === $jumlahGenap) {
            $sementara['kanan'] = 'RATA';
        } elseif ($jumlahGanjil > $jumlahGenap) {
            $sementara['kanan'] = 'GANJIL';
        } else {
            $sementara['kanan'] = 'GENAP';
        }


        if ($jumlahKiri == $jumlahKanan) {
            $sementara['kiri'] = 'RATA';
        } else if ($jumlahKiri > $jumlahKanan) {
            $sementara['kiri'] = 'KECIL';
        } else {
            $sementara['kiri'] = 'BESAR';
        }
        // $sementara['kanan'] =($jumlahGanjil > $jumlahGenap) ? 'GANJIL' : ($jumlahGanjil < $jumlahGenap) ? 'GENAP' : 'RATA';
        // $sementara['kiri'] = ($jumlahKiri > $jumlahKanan) ? 'KECIL' : ($jumlahKiri < $jumlahKanan) ? 'BESAR' : 'RATA';
        $sementara['jumlahganjil'] = $jumlahGanjil;
        $sementara['jumlahgenap'] = $jumlahGenap;
        return $sementara;
    }

    public static function olahAskopArray($array)
    {
        // return $array;

        $besar = (object) ['nomor' => 0, 'jlh' => 0];
        $kecil = (object) ['nomor' => 9, 'jlh' => 9];


        $valBesar = 0;
        $valKecil = 0;
        foreach ($array as $d) {

            if ($d->jlh > $besar->jlh) {
                $besar = $d;
            }
            if ($d->jlh < $kecil->jlh) {
                $kecil = $d;
            }
        }

        return [
            'besar' => $besar,
            'kecil' => $kecil
        ];
    }


    public static function olahPermutasi($limit = 2)
    {
        // $data =  self::permutasi(self::arrayAngka($angka));
        $res = [];
        foreach (self::$datapermutasi as $data3d) {

            $res[] =  Str::limit($data3d, $limit, '');
        }

        // $res = self::arrayAngka($angka);
        // return implode(', ', $res);
        return implode(', ', array_unique($res));
    }
    // public static function cari2DAngkaMain($angka)
    // {
    //     $arr = self::arrayAngka($angka);
    //     $res = [];

    //     foreach ($arr as $item) {
    //         for ($i = 0; $i < count($arr); $i++) {
    //             if ($item !== $arr[$i]) {

    //                 $res[] = $item . $arr[$i];
    //             }
    //         }
    //     }
    //     $res[] = end($arr) . end($arr);

    //     // foreach ($arr as $a) {

    //     //     foreach ($arr as $b) {
    //     //         // if ($a == $b) {
    //     //         //     continue;
    //     //         // }


    //     //         $res[] = $a . $b;
    //     //     }
    //     // }

    //     return implode(', ', array_unique($res));


    //     return implode(', ', array_unique($res));
    // }
    // public static function cari3DAngkaMain($angka)
    // {

    //     $data =  self::permutasi(self::arrayAngka($angka));
    //     $res = [];
    //     foreach(self::$data3d as $data3d){

    //      $res[] =  Str::limit($data3d,3,'');
    //     }

    //     // $res = self::arrayAngka($angka);
    //     // return implode(', ', $res);
    //     return implode(', ', array_unique($res));
    // }

    public static function  permutasi($items, $perms = array())
    {

        if (empty($items)) {
            // $res[] = join('', $perms);
            // print_r(join('', $perms));
            self::$datapermutasi[] = join('', $perms);
            // return  join('', $perms) . "\n";
        } else {
            for ($i = count($items) - 1; $i >= 0; --$i) {
                $newitems = $items;
                $newperms = $perms;
                list($foo) = array_splice($newitems, $i, 1);
                array_unshift($newperms, $foo);

                // $res[] = join('', $newitems) ;
                self::permutasi($newitems, $newperms);
            }
        }

        // print_r($res);
    }
    // public static function cari4DAngkaMain($angka)
    // {
    //     $arr = self::arrayAngka($angka);
    //     $res = [];

    //     foreach ($arr as $item) {

    //         for ($i = 0; $i < count($arr); $i++) {
    //             for ($x = 0; $x < count($arr); $x++) {
    //                 for ($y = 0; $y < count($arr); $y++) {
    //                     if ($item !== $arr[$i] && $item !== $arr[$x] && $item !== $arr[$y] &&  $arr[$i] !== $arr[$x] &&  $arr[$i] !== $arr[$y] &&  $arr[$x] !== $arr[$y]) {
    //                         $res[] = $item . $arr[$i] . $arr[$x] . $arr[$y];
    //                     }
    //                 }
    //             }
    //         }
    //     }

    //     return implode(', ', $res);
    // }
    private static function arrayAngka($angka)
    {
        return [$angka[0], $angka[1], $angka[2], $angka[3]];
    }
    public static function cariAnjuranBolakBalikAngkaMain($angka)
    {
        /**
         * Rumus Anjuran Bolak Balek: ambil angka ke dua , ke tiga dan terakhir Angka Main maka penulisan
         * Anjuran Bolak Balek: 279
         * Ada angka kembar disini tidak masalah misal : 299 atau 992 atau 929
         * yang jadi masalah kalau 3 angka sama misal 999 , maka penulisan Anjuran Bolak Balek : simbol minus (-)
         *
         */



        $anjuran = $angka[1] . $angka[2] . $angka[3];
        if ($angka[1] == $angka[2] && $angka[1] === $angka[3] && $angka[2] == $angka[3]) {
            return '-';
        }

        return $anjuran;
    }
    public static function cariColokMacauAngkaMain($angka)
    {
        /**
         * Colok Macau : kombinasikan Angka ke Dua dan ketiga , kemudian kombinasikan angka ke Dua dan Terakhir
         * Contoh penulisan
         * Colok Macau: 27, 29
         * Apabila angka kedua dan ketiga sama maka tidak usah ditampilkan , begitu pula apabila angka kedua dan keempat sama maka tidak usa ditampilkan juga
         * Bagaimana kalau angka main 1 8 8 8?
         * Maka tidak ada prediksi Colok Macau atau ditulis dengan simbol minus (-)
         */
        $colokMacau = [];
        if ($angka[1] !== $angka[2]) {
            $colokMacau[] =  $angka[1] . $angka[2];
        }
        if ($angka[1] !== $angka[3]) {
            $colokMacau[] =  $angka[1] . $angka[3];
        }
        return implode(', ', array_unique($colokMacau));
    }
    public static function cariColokBebasAngkaMain($angka)
    {
        /**
         * Colok Bebas : ambil angka ke dua dan terakhir dari Angka Main
         * Contoh penulisan
         * Colok Bebas: 2, 9
         * Apabila angka pertama dan angka terakhir sama , maka tampilkan satu angka saja
         *
         */
        $angkaEkor = [$angka[1], $angka[3]];
        return implode(', ', array_unique($angkaEkor));
    }

    public static function genapGanjil($angka)
    {
        $angka = $angka[2] . $angka[3];
        $status = [];


        if ($angka >= 50) {
            $status[] = 'BESAR';
        } else {
            $status[] = 'KECIL';
        }

        if ($angka % 2 == 0) {
            $status[] = 'GENAP';
        } else {
            $status[] = 'GANJIL';
        }

        return $status;
    }
    public static function cariEkorAngkaMain($angka)
    {

        /**
         *
         * Rumus EKOR : ambil angka pertama dan angka terakhir dari Angka Main
         * Contoh penulisan
         * Ekor: 1 dan 9
         * Apabila angka pertama dan angka terakhir sama , maka tampilkan satu angka saja
         *
         */
        $angkaEkor = [$angka[0], $angka[3]];
        return implode(', ', array_unique($angkaEkor));
    }
    public static function cariDasarAngkaMain($angka)
    {
        //    return 'ahahah';


        $jumlah = $angka[2] + $angka[3];
        $jumlah =  (string) $jumlah;
        $angka = $angka[2] . $angka[3];

        // bypass jumlah
        // $jumlah = '18';




        if (strlen($jumlah) > 1) {

            $angka = $jumlah[0] + $jumlah[1];
        } else {

            $angka = $jumlah;
        }



        /**
         * Rumus Dasar: ambil dari dua angka terakhir Angka Main maka
         * Dasar: Besar, Ganjil
         */
        $status = [];


        if ($angka >= 5) {
            $status[] = 'BESAR';
        } else {
            $status[] = 'KECIL';
        }

        if ($angka % 2 == 0) {
            $status[] = 'GENAP';
        } else {
            $status[] = 'GANJIL';
        }
        // if ($angka >= 50) {
        //     $status[] = 'BESAR';
        // } else {
        //     $status[] = 'KECIL';
        // }

        // if ($angka % 2 == 0) {
        //     $status[] = 'GENAP';
        // } else {
        //     $status[] = 'GANJIL';
        // }


        return implode(', ', $status);
    }
    public static function cariShioAngkaMain($angka)
    {
        /**
         * Untuk rumus prediksi lainnya, patokan berdasarkan Angka Main yang didapat , misalkan Angka Main :
         * 1 2 7 9
         * maka
         * Rumus Prediksi Shio : ambil dua angka terakhir dari Angka Main sesuaikan dengan tabel SHIO
         * Shio: Kuda*
         *
         */
        $angka = substr($angka, 2);
        $angka = str_pad($angka, 2, '0', STR_PAD_LEFT);

        $arrayShio = [
            "01" => "TIKUS", "02" => "BABI", "03" => "ANJING", "04" => "AYAM", "05" => "MONYET", "06" => "KAMBING", "07" => "KUDA", "08" => "ULAR", "09" => "NAGA", "10" => "KELINCI", "11" => "HARIMAU", "12" => "KERBAU", "13" => "TIKUS", "14" => "BABI", "15" => "ANJING", "16" => "AYAM", "17" => "MONYET", "18" => "KAMBING", "19" => "KUDA", "20" => "ULAR", "21" => "NAGA", "22" => "KELINCI", "23" => "HARIMAU", "24" => "KERBAU", "25" => "TIKUS", "26" => "BABI", "27" => "ANJING", "28" => "AYAM", "29" => "MONYET", "30" => "KAMBING", "31" => "KUDA", "32" => "ULAR", "33" => "NAGA", "34" => "KELINCI", "35" => "HARIMAU", "36" => "KERBAU", "37" => "TIKUS", "38" => "BABI", "39" => "ANJING", "40" => "AYAM", "41" => "MONYET", "42" => "KAMBING", "43" => "KUDA", "44" => "ULAR", "45" => "NAGA", "46" => "KELINCI", "47" => "HARIMAU", "48" => "KERBAU", "49" => "TIKUS", "50" => "BABI", "51" => "ANJING", "52" => "AYAM", "53" => "MONYET", "54" => "KAMBING", "55" => "KUDA", "56" => "ULAR", "57" => "NAGA", "58" => "KELINCI", "59" => "HARIMAU", "60" => "KERBAU", "61" => "TIKUS", "62" => "BABI", "63" => "ANJING", "64" => "AYAM", "65" => "MONYET", "66" => "KAMBING", "67" => "KUDA", "68" => "ULAR", "69" => "NAGA", "70" => "KELINCI", "71" => "HARIMAU", "72" => "KERBAU", "73" => "TIKUS", "74" => "BABI", "75" => "ANJING", "76" => "AYAM", "77" => "MONYET", "78" => "KAMBING", "79" => "KUDA", "80" => "ULAR", "81" => "NAGA", "82" => "KELINCI", "83" => "HARIMAU", "84" => "KERBAU", "85" => "TIKUS", "86" => "BABI", "87" => "ANJING", "88" => "AYAM", "89" => "MONYET", "90" => "KAMBING", "91" => "KUDA", "92" => "ULAR", "93" => "NAGA", "94" => "KELINCI", "95" => "HARIMAU", "96" => "KERBAU", "97" => "TIKUS", "98" => "BABI", "99" => "ANJING", "00" => "AYAM",

        ];

        return $arrayShio[$angka];
    }


    private static function hitungAngkaMain($duaangkaterakhir)
    {

        // return $duaangkaterakhir;
        /**
         *
         * Rumus untuk Angka Main
         * Ambil 2 periode sebelumnya (dijumlahkan)
         * contoh :
         * 1 2 3 4
         * 6 7 8 9
         * ditotal =
         * 8 0 2 3
         * masing masing kurangin 1 (khusus untuk 0 mundur jadi 9)
         * maka =
         * 7 9 1 2
         *
         * Apabila angka melebihi 5 seperti : 8 9 7 0 + 3 9 0 1 = 1 2 8 7 1
         * Angka kelima kurangi angka keempat ( 1 dikurang 7 ) apabila minus , ilangkan saja minusnya
         * jadinya 1 2 8 6 , kurangin satu sesuai rumus
         * maka angka main = 0 1 7 5
         *
         * Apabila angka main kurang dari 4 seperti : 0 1 2 3 + 0 3 6 9 = 4 9 2
         * maka angka pertama adalah angka kedua dikurangin / dimundurin satu jadi :
         * 3 4 9 2 terus masing-masing angka dikurangin satu sesuai rumus jadinya
         * 2 3 0 1 <- angka main
         *
         *
         *
         */



        $arr = [
            "1" => "0",
            "-1" => "0",
            "-2" => "1",
            "2" => "1",
            "-3" => "2",
            "3" => "2",
            "-4" => "3",
            "4" => "3",
            "-5" => "4",
            "5" => "4",
            "-6" => "5",
            "6" => "5",
            "-7" => "6",
            "7" => "6",
            "-8" => "7",
            "8" => "7",
            "-9" => "8",
            "9" => "8",
            "0" => "9",
        ];

        $angkaMain = 0;
        foreach ($duaangkaterakhir as $angka) {
            $angkaMain = $angkaMain +   str_pad($angka->keluaran, 4, "0", STR_PAD_LEFT);
        }



        // $angkaMain = 492;


        $angkaMain =  (string) $angkaMain;
        $panjangAngkaMain =  strlen($angkaMain);
        switch ($panjangAngkaMain) {
            case '3':
                $angkaMain =  $arr[$angkaMain[0] - 1] . $arr[$angkaMain[0]] . $arr[$angkaMain[1]] . $arr[$angkaMain[2]];
                break;
            case '4':
                $angkaMain =  $arr[$angkaMain[0]] . $arr[$angkaMain[1]] . $arr[$angkaMain[2]] . $arr[$angkaMain[3]];
                break;
            case '5':
                $xxx = $angkaMain[4] - $angkaMain[3];

                $angkaMain =  $arr[$angkaMain[0]] . $arr[$angkaMain[1]] . $arr[$angkaMain[2]] . $arr[$xxx];
                break;

            default:
                # code...
                break;
        }

        return $angkaMain;
    }

    private static function kurangSatu($angka)
    {

        $angkaKurangSatu = $angka[0];
    }
}
