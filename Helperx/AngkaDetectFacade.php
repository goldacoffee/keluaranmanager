<?php

namespace Helperx\Facades;
use Illuminate\Support\Facades\Facade;

class AngkaDetect extends Facade{

    protected static function getFacadeAccessor()
    {
        return 'AngkaDetect';
    }
}