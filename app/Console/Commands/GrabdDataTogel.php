<?php

namespace App\Console\Commands;

use App\Http\Controllers\TogelGrabberController as TogelGrabber;
use Illuminate\Console\Command;


class GrabdDataTogel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'togel:grab {provider?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ambil data dari situs provider langsung';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $providerName =  $this->argument('provider');

        $x = new TogelGrabber;
        $data  = $x->doGrab($providerName);
        // $data  = $x->detectGrab($providerName);
        // dd($data);

        print_r($data);
    }
}
