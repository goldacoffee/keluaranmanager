<?php

namespace App\Http\Controllers;

use App\Model\DataAKKE;
use Illuminate\Http\Request;

class DataAKKEController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\DataAKKE  $dataAKKE
     * @return \Illuminate\Http\Response
     */
    public function show(DataAKKE $dataAKKE)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\DataAKKE  $dataAKKE
     * @return \Illuminate\Http\Response
     */
    public function edit(DataAKKE $dataAKKE)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\DataAKKE  $dataAKKE
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataAKKE $dataAKKE)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\DataAKKE  $dataAKKE
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataAKKE $dataAKKE)
    {
        //
    }
}
