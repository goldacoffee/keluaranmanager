<?php

namespace App\Http\Controllers;

use App\Model\DataColokBebas;
use Illuminate\Http\Request;

class DataColokBebasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\DataColokBebas  $dataColokBebas
     * @return \Illuminate\Http\Response
     */
    public function show(DataColokBebas $dataColokBebas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\DataColokBebas  $dataColokBebas
     * @return \Illuminate\Http\Response
     */
    public function edit(DataColokBebas $dataColokBebas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\DataColokBebas  $dataColokBebas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataColokBebas $dataColokBebas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\DataColokBebas  $dataColokBebas
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataColokBebas $dataColokBebas)
    {
        //
    }
}
