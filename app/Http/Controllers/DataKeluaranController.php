<?php

namespace App\Http\Controllers;

use App\Model\DataKeluaran;
use Illuminate\Http\Request;

class DataKeluaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\DataKeluaran  $dataKeluaran
     * @return \Illuminate\Http\Response
     */
    public function show(DataKeluaran $dataKeluaran)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\DataKeluaran  $dataKeluaran
     * @return \Illuminate\Http\Response
     */
    public function edit(DataKeluaran $dataKeluaran)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\DataKeluaran  $dataKeluaran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataKeluaran $dataKeluaran)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\DataKeluaran  $dataKeluaran
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataKeluaran $dataKeluaran)
    {
        //
    }
}
