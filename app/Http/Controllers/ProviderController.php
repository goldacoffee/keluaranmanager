<?php

namespace App\Http\Controllers;

use App\Model\DataKeluaran;
use Illuminate\Http\Request;
use App\Model\DataProvider;
use Helperx\AngkaDetect;

class ProviderController extends Controller
{
    public function Index(Request $request)
    {
        $data = DataProvider::orderBy('position', 'asc')->get();
        return response($data, 200);
    }
    public function Add(Request $request)
    {
        $baru = new DataProvider;
        $baru->nama =  $request->nama;
        $baru->kode = strtoupper($request->kode);
        if (isset($request->gambar)) {
            $baru->gambar =  $request->gambar;
        }

        if (isset($request->keterangan)) {

            $baru->keterangan =  $request->keterangan;
        }

        try {
            $data = $baru->save();
            return response($data, 200);
        } catch (\Throwable $th) {
            return response($th->getMessage(), 401);
        }
    }
    public function Details(Request $request, $id)
    {

        try {
            $data =  DataProvider::where('id', $id)->first();
            if (!$data) {
                return response('error', 401);
            }

            return response($data, 200);
        } catch (\Throwable $th) {
            return response('error', 401);
        }
    }
    public function Delete(Request $request)
    {

        try {
            $delete = DataProvider::where('id', $request->id);
            $delete->delete();
            return response($delete, 200);
        } catch (\Throwable $th) {
            return response($th->getMessage(), 401);
        }
    }
    public function Update(Request $request)
    {

        try {
            $update = DataProvider::where('id', $request->id)
                ->update([
                    'kode' => strtoupper($request->kode),
                    'nama' => $request->nama,
                    'gambar' => $request->gambar,
                    'hari_buka' => $request->hari_buka,
                    'jam_buka' => $request->jam_buka,
                    'jam_tutup' => $request->jam_tutup,
                    'keterangan' => $request->keterangan
                ]);

            return response($update, 200);
        } catch (\Throwable $th) {
            return $th->getMessage();
            return response('Failed To Update', 401);
        }
    }

    public function changeOrder(Request $request)
    {
        // return $request->data;
        // $request = (array) $request;
        try {
            foreach ($request->data as $key => $req) {
                $num = $key + 1;
                DataProvider::where('id', $req)
                    ->update(['position' => $num]);
            }
            return response()->json(['success' => true,], 200);
        } catch (\Throwable $th) {
            return response()->json(['success' => false,], 200);
            //throw $th;
        }
    }

    public function prediksi( $provider,$hari = 'senin', $tanggal)
    {
        // return [
        //     'provider'=>$provider,
        //     'hari'=>$hari,
        //     'tanggal'=>$tanggal
        // ];




        return AngkaDetect::hitungPrediksi($provider,$tanggal);


        // return response($res, 200);
    }
}
