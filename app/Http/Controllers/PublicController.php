<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\DataKeluaran;
use App\Model\DataProvider;

class PublicController extends Controller
{
    public function getAllData(Request $request)
    {


        // $data =   DataProvider::with('limit5')->get();
        if (isset($request->secret)) {

            $domain = explode('://', base64_decode($request->secret))[1];
            if ($domain == 'mafiahasil.top') {
                $data =   DataProvider::orderBy('position', 'asc')->get()->loadUsingLimit('datakeluaran');
            } else {

                $allowed = $this->webData($domain);

                $data =   DataProvider::whereIn('kode', $allowed)->orderBy('position', 'asc')->get()->loadUsingLimit('datakeluaran');
            }
        } else {
            $data =   DataProvider::orderBy('position', 'asc')->get()->loadUsingLimit('datakeluaran');
        }


        // $data = DataProvider::first()->with('DataKeluaran');
        // $data = DataProvider::with(
        //     [
        //         'DataKeluaran' => function ($query) {
        //             $query->take(5);
        //         }
        //     ]
        // )->get();

        return response()->json($data, 200);
    }

    private function webData($domain)
    {

        $array = array(
            'lumbungprediksi.com' => [
                'SD',
                'HK',
                'SGP',
                'HKD',
                // 'BUL', // removed by request on 22-08-2020
                'SYD',
                // 'TH', // change to taiwan 17 juni 2020
                'TW',
                'SGP45',
                'SGM',
                'MY',
                'QTR',
                'MAC',
            ]
        );


        return $array[$domain];
    }
}
