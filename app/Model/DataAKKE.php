<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DataAKKE extends Model
{
    protected $table = 'data_akke';
    protected $fillable = [
        'id_keluaran',
        'as',
        'kop',
        'kepala',
        'ekor',
    ];
}
