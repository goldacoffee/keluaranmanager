<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class DataProvider extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'data_provider';
    protected $fillable = [
        'nama',
        'gambar',

        'keterangan',
        'position'
    ];
    protected $casts = [
        'hari_buka' => 'array',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    protected $appends = [
        'banner'
    ];


    public function getBannerAttribute()
    {



        $array = array(
            "Bangkok" => "https://1.bp.blogspot.com/-2It1L5OQHIo/Xn9-NGFhToI/AAAAAAAAJvo/ryRdRN738rQAY_obzkSirqnWSkKqVrErgCLcBGAsYHQ/s1600/PREDIKSI%2BBANGKOK.jpg",
            "Barcelona" => "https://1.bp.blogspot.com/-cwlkbSb1khw/Xn9-NOpFruI/AAAAAAAAJvk/xVohYELdoQY-kdMzJ8aK3Qb87kG7UbdnACLcBGAsYHQ/s1600/PREDIKSI%2BBARCELONA.jpg",
            "Beijing" => "https://1.bp.blogspot.com/-L_Z_X7CGdGw/Xn9-NObummI/AAAAAAAAJvg/k4ZQyB4LTzIquez5VfHuroTdypDve_lFgCLcBGAsYHQ/s1600/PREDIKSI%2BBEIJING.jpg",
            "Berlin" => "https://1.bp.blogspot.com/-Smz-Z-jj4UA/Xn9-QBoz6UI/AAAAAAAAJvs/Mcb2tflv_do-z0X04cKwyOfHOsa2a4W5QCLcBGAsYHQ/s1600/PREDIKSI%2BBERLIN.jpg",
            "Bullseye" => "https://1.bp.blogspot.com/-SAOXytZCZeg/Xn9-QHf9V9I/AAAAAAAAJvw/LklWwudygPYN1uAUrSrhWkK1jrTh42vRgCLcBGAsYHQ/s1600/PREDIKSI%2BBULLSEYE.jpg",
            "China" => "https://1.bp.blogspot.com/-QdImkHAG0ls/Xn9-Qi0t6TI/AAAAAAAAJv0/q8Whg9jx-JcrCByZT0KsG6NperkPCOwigCLcBGAsYHQ/s1600/PREDIKSI%2BCHINA.jpg",
            "Hongkong" => "https://1.bp.blogspot.com/-6eOkXRyzMwo/Xn9-UJhbPMI/AAAAAAAAJv8/nkTsBgO67kEPwus0ZtBhThvvpws5iNJGQCLcBGAsYHQ/s1600/PREDIKSI%2BHONGKONG.jpg",
            "Japan" => "https://1.bp.blogspot.com/-OAzZ8hLWF1Q/Xn9-UL7_tHI/AAAAAAAAJv4/10ASBC4KyssS2p5yITJz0RgSW1MZnSMTQCLcBGAsYHQ/s1600/PREDIKSI%2BJAPAN.jpg",
            "Macau" => "https://1.bp.blogspot.com/-3BohJi6jXbE/Xn9-UETexnI/AAAAAAAAJwA/FpXQbcaPKPI3jpotI2H8z7nj-7boLWT2ACLcBGAsYHQ/s1600/PREDIKSI%2BMACAU.jpg",
            "Magnum4D" => "https://1.bp.blogspot.com/-HWLa6HAWRoU/Xn9-V9VwbwI/AAAAAAAAJwE/D20Lze7Wrj8DftnlF1Vulix6a73JbfzsQCLcBGAsYHQ/s1600/PREDIKSI%2BMAGNUM4D.jpg",
            "Malaysia" => "https://1.bp.blogspot.com/-H-FShJfGSy8/Xn9-WQrGG-I/AAAAAAAAJwI/V32IrYxEbAUyqxaFDB5Swwsh0wv5p2giACLcBGAsYHQ/s1600/PREDIKSI%2BMALAYSIA.jpg",
            "Manila" => "https://1.bp.blogspot.com/-6tGwSz0FEcg/Xn9_O6HGmkI/AAAAAAAAJwo/T4vrmC_g7Xs0H6qshknIn4Sl0px677RmwCLcBGAsYHQ/s1600/PREDIKSI%2BMANILA.jpg",
            "Milan" => "https://1.bp.blogspot.com/-q48nObHZOKM/Xn9_O7KSAoI/AAAAAAAAJwk/5gQVKop0MA4KqSDHsc3VIQWnCf_HNNIFQCLcBGAsYHQ/s1600/PREDIKSI%2BMILAN.jpg",
            "PCSO" => "https://1.bp.blogspot.com/-bfoQiZnVDCM/Xn9_PW3OkYI/AAAAAAAAJws/ZY4xshKxSJ8s4yT0zLl16SAPoz-s11rTQCLcBGAsYHQ/s1600/PREDIKS%2BPCSO.jpg",
            "Qatar" => "https://1.bp.blogspot.com/M3vCY2UquZE/Xn9_T7Ox_TI/AAAAAAAAJww/WsF4gGJo28Y_V0Ybxazw06Ri15XTsjf6QCLcBGAsYHQ/s1600/PREDIKSI%2BQATAR.jpg",
            "Seoul" => "https://1.bp.blogspot.com/-2srsaD0KfSg/Xn9_VE_XGEI/AAAAAAAAJw4/xbtUdk8W93s6U283iR9khCJ7TgcRjmEKACLcBGAsYHQ/s1600/PREDIKSI%2BSEOUL.jpg",
            "Singapore45" => "https://1.bp.blogspot.com/-1DZMjXxNvfk/Xn9_V6bxwtI/AAAAAAAAJw8/CaPPSPR4hMk7a1zCbKYLi466ioYvLy0iACLcBGAsYHQ/s1600/PREDIKSI%2BSINGAPORE45.jpg",
            "Singapore" => "https://1.bp.blogspot.com/-kp0VXIR1Lfk/Xn9_U2U_5eI/AAAAAAAAJw0/rx8kev9r5hEXP36vxisl0g-H8N_yivGNQCLcBGAsYHQ/s1600/PREDIKSI%2BSINGAPORE.jpg",
            "Sydney" => "https://1.bp.blogspot.com/-2tu8wuIsTuM/Xn9_Yr-9j6I/AAAAAAAAJxA/4HD1YtJvFHElIottiYpt4SdtNBQ9_gUoACLcBGAsYHQ/s1600/PREDIKSI%2BSYDNEY.jpg",
            "Taiwan" => "https://1.bp.blogspot.com/-h5ItBl-6Lr8/Xn9_Zob2EvI/AAAAAAAAJxE/OrFe_rnDI0Avm-lmRGpQdnPdx4g6AGhzQCLcBGAsYHQ/s1600/PREDIKSI%2BTAIWAN.jpg",
            "Timor" => "https://1.bp.blogspot.com/-S37xup7gOqs/Xn9_a7VlhfI/AAAAAAAAJxI/gbwwx6fKX6otKudmSTFkBZDRvUPKBfFmQCLcBGAsYHQ/s1600/PREDIKSI%2BTIMOR.jpg",
            "Tokyo" => "https://1.bp.blogspot.com/-rGcK9dSPZVI/Xn9_bRIJMzI/AAAAAAAAJxM/8Ch79ngvBwUktP69I-KSqzr5A9lV49fIACLcBGAsYHQ/s1600/PREDIKSI%2BTOKYO.jpg",
            "Cambodia" => "https://1.bp.blogspot.com/-CFJoz_eOX5E/Xp2_ILHEC3I/AAAAAAAAKG4/M4MmrAEHBpcLM8z4ysHifIN9jv808eXEwCLcBGAsYHQ/s1600/PREDIKSI%2BCAMBODIA.jpg",
            "Thailand" => "https://1.bp.blogspot.com/-WSix4hjxAdQ/XqlJ-26-T2I/AAAAAAAAKPI/fD9Jok51sDAHuv_k4y0lsXM3jVx-fS0zgCLcBGAsYHQ/s1600/PREDIKSI%2BTHAILAND.jpg",

            "HKD" => "https://1.bp.blogspot.com/-7GZcw_WBqJc/Xqlhu69XA3I/AAAAAAAAKQw/3mu9Z33rMscCh__DD3DRbOjQmL4MoFSgQCLcBGAsYHQ/s1600/PREDIKSI%2BHK%2BSIANG.jpg",
            "SGM" => "https://1.bp.blogspot.com/-X5kjA2tROHk/XqlhWUINSmI/AAAAAAAAKQY/C6uF-Ns3QgEhsaTvQShhziDPHdPIvuGTwCLcBGAsYHQ/s1600/PREDIKSI%2BSG%2BMETRO.jpg",
            "SYD" => "https://1.bp.blogspot.com/-NqKLjYCBsuM/XqlhX9ogWnI/AAAAAAAAKQg/RD7dmpPOAfQKM0uFap3wFHiL4S88Lw0SQCLcBGAsYHQ/s1600/PREDIKSI%2BSYDNEY4D.jpg",
            "MYD" => "https://1.bp.blogspot.com/-AxPyx0Nlg24/XqlhWpIHPGI/AAAAAAAAKQc/U2_z5Zr8eKUlXeLe8r7tdQxCwlNaLkYeQCLcBGAsYHQ/s1600/PREDIKSI%2BMALAYSIA%2BSIANG.jpg",
            "MAC" => "https://1.bp.blogspot.com/-6Vgt-njvBAQ/X0WidsAIg0I/AAAAAAAAMP8/Cyn096brmccQPvyUqjZt0Csr9Cknk7X1wCLcBGAsYHQ/s1600/PREDIKSIMACAUPOOLS.jpg",

        );

        if (array_key_exists($this->attributes['nama'], $array)) {
            return $array[$this->attributes['nama']];
        } elseif (array_key_exists($this->attributes['kode'], $array)) {
            return $array[$this->attributes['kode']];
        } else {
            return 'https://1.bp.blogspot.com/-CFJoz_eOX5E/Xp2_ILHEC3I/AAAAAAAAKG4/M4MmrAEHBpcLM8z4ysHifIN9jv808eXEwCLcBGAsYHQ/s1600/PREDIKSI%2BCAMBODIA.jpg';
        }
    }

    public function keluaran()
    {
        return  $this->hasMany('App\Model\DataKeluaran', 'provider', 'id');
    }
    public function DataKeluaran(int $count = 6): HasMany
    {
        return $this->keluaran()->orderBy('tanggal', 'DESC')->limit($count);
    }
}
