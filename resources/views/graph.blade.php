<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>

<body>
    <div class="frequency-cointainer">
        <span class="frequency-statistik">Statistik Keluaran Singapore [30 Undian]</span>

        @foreach ($data->_askor as $key => $value)
        
        <div class="frequency-card">
            <div class="position">{{ strtoupper($key) }} </div>
            <div class="frequency-graph">
                
                @foreach ($value['data'] as $index => $valuex)
                <div class="frequency-bar">
                    <div class="bar" id="bar-0-0" div="" style="height: {{ ($valuex * 100) / 10 }}%;">
                        <div>{{ ($valuex != 0 ) ? $valuex : '' }}</div>
                    </div>
                    <div class="number"> {{ $index }} </div>
                </div>
                @endforeach

            </div>
            <br>
            {{-- {{ print_r($value) }} --}}
            <table class="prediksi-table">
                <tbody>
                    <tr>
                        <th colspan="2">Mayoritas Angka</th>
                    </tr>
                    <tr>
                        <td>{{ $value['kiri'] }}</td>
                        <td>{{ $value['kanan'] }}</td>

                    </tr>
                </tbody>
            </table>
        </div>

        @endforeach
    </div>






</body>

</html>